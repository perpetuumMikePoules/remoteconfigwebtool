# top level script for tool

# define Flask rct instance
from app import rct, db
from app.models import DcInfo, DcConfig

# create a shell context for use with 'flask shell' command
@rct.shell_context_processor
def make_shell_context() -> dict:
    # register items in xhell context
    return {'db':db, 'DcInfo':DcInfo, 'DcConfig':DcConfig}
