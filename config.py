import os

# get main directory of application (ie this file's directory)
basedir: str = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    # Congiuration items are generally set from environmental variables, a fallback is provided if environmental variable isn't defined

    # Flask system cryptograhic key
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'zorro-zorro-1'

    # Database URL (location)
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    # Disable Flask-SQLAlchemy's signal app on db change
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Development build uses local host and local SFTP server
    DEV_BUILD = os.environ.get('DEV_BUILD') or False