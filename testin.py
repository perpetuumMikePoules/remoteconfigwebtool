
import string

# hstr = '09ag'
# if any(char not in string.hexdigits for char in hstr):
    
#     print('Bad')
# else:
#     print('OK')
# octets format - nn:nn:nn:nn:nn:nn


def filter_validation(filter: str) -> None:
    octets: List[str] = filter.split(":")
    # check number of octets
    if len(octets) != 6:
#        raise ValidationError('Incorrect format. [nn:nn:nn:nn:nn:nn]')
        print('Incorrect format. [nn:nn:nn:nn:nn:nn]')
        return
    
    # check value of each octet
    for nn in octets:
        if len(nn) != 2:
            print('Incorrect Octet. [nn]')
            return
        for digit in nn:
            if digit not in string.hexdigits:
                if digit != 'x' and digit != 'X' and digit != '-':
#                    raise ValidationError('Incorrect octet value. [00 - FF]')
                    print('Incorrect octet value. [ 0-9, a-F, x, - ]')
                    return


filter: str = '01:23:45:--:X-:xF'

filter_validation(filter)

