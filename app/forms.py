# Flask-WTF uses Pyhton classes to represent forms, which can be added to an HTML templateso it can be rendered on web page
# A form class simply defines the fields of the form as a class variables, which know how to render themselves as HTML
# Fields first argument is a description label.
#    Optional 'validators' argument can be used to attch validation bvehaviours to fields
import string
from flask_wtf import FlaskForm
from wtforms import BooleanField, SubmitField, IntegerField, StringField, DateTimeField, SelectField, FieldList, FormField, SubmitField, Form, FormField, TextAreaField
from wtforms.validators import ValidationError, DataRequired, NumberRange, Optional, Required

from typing import List

# "nnnn-nnnn-nnnn-nnnn;" 
def device_id_check(form, field: str) -> None:
    # split id into sub elements
    elements: List[str] = field.data.split("-")
    # check number of elements
    if len(elements) != 4:
        raise ValidationError('Incorrect format. [nnnn-nnnn-nnnn-nnnn]')
        return

    # check value of each element
    for nnnn in elements:
        # four sub elements
        if len(nnnn) != 4:
            raise ValidationError('Incorrect number of elemnts. [nnnn-nnnn-nnnn-nnnn]')
            return
        # all chars are hex digits
        if  any(char not in string.hexdigits for char in nnnn):
            raise ValidationError('Not a hex digit. [nnnn-nnnn-nnnn-nnnn]')
            return

# octets format - nn:nn:nn:nn:nn:nn
def filter_check(form, field: str) -> None:
    octets: List[str] = field.data.split(":")
    # check number of octets
    if len(octets) != 6:
        raise ValidationError('Incorrect format. [nn:nn:nn:nn:nn:nn]')
        return
    
    # check value of each octet
    for nn in octets:
        if len(nn) != 2:
            raise ValidationError('Incorrect Octet. [nn]')
            return
        for digit in nn:
            if digit not in string.hexdigits:
                if digit != 'x' and digit != 'X' and digit != '-':
                    raise ValidationError('Incorrect octet value. [ 0-9, a-F, x, - ]')
                    return
            
# APNNOR:<num>:<name>:<address>:<username>:<password>
def access_point_check(form, field: str)-> None:
    apn: List[str] = field.data.split(":")
    if len(apn) != 5:
        raise ValidationError('Incorrect format. [<num>:<name>:<address>:<username>:<password>]')

class SystemForm(Form):
    sd_card = SelectField('SD Card Storage enable:', choices=[('0','No Change'),('1','true'), ('2','false')], coerce=int, validators=[Optional()])
    log_event_enable = SelectField('Extra event logging:', choices=[('3','No Change'),('0','Normal'),('1','WSN sniffing'), ('2','GSM OPerator sniffing')], coerce=int, validators=[Optional()])
    remote_restart = BooleanField('Remote start request:', validators=[Optional()])      # command only
    stored_data_erase =BooleanField('Erase stored data:', validators=[Optional()])    # command only

class CommsForm(Form):
    azure_setting = SelectField('Azure Cloud Setting:', choices=[('3','No Change'),('0','Perpetuum'), ('1','Special')], coerce=int, validators=[Optional()])
    access_point = StringField('GSM APN:', validators=[Optional(), access_point_check])
    single_endpoint = SelectField('Single endpoint enable:', choices=[('0','No Change'),('1','true'), ('2','false')], coerce=int, validators=[Optional()])
    data_upload_window = IntegerField('Data upload window time (5 - 55 mins):', validators=[NumberRange(min=5, max=55), Optional()])
    modem_highspeed = SelectField('GEM Modem high speed:', choices=[('0','No Change'),('1','true'), ('2','false')], coerce=int, validators=[Optional()])
    bb_blocks_per_chunk = IntegerField('BB Data, blocks per chunk (1 - 5):', validators=[NumberRange(min=1, max=5), Optional()])

class BumpboxForm(Form):
    bb_gps_enable = SelectField('GPS enable:', choices=[('0','No Change'),('1','true'), ('2','false')], coerce=int, validators=[Optional()])
    bb_data_enable = SelectField('Data gathering enable:', choices=[('0','No Change'),('1','true'), ('2','false')], coerce=int, validators=[Optional()])
    bb_data_type = SelectField('Data gathering message type:', choices=[('0','No Change'),
        ('2','Fixed HiFi'),
        ('4','RMS Only'),
        #'5','Variable HiFi'),
        #('6','Fixed HiFi, GPS Ints'),
        #('7','Variable HiFi, GPS Ints'),
        #('8','RMS Only, GPS Ints'),
        ('9','RMS2, BS ride index '),
        #('10','RMS2, GPS Ints, BS ride index'),
        ('11','RMS2, SC ride index'),
        #('12','RMS2, GPS Ints, SC ride index'),
        ('13','RMS2, SQ ride index'),
        #('14','RMS2, GPS Ints, SQ ride index')
        ], coerce=int, validators=[Optional()])
    bb_decimation = IntegerField('Data decimation (1[Off] - 255):', validators=[NumberRange(min=1, max=255), Optional()])
    bb_accel_axis_config = SelectField('Accelerator axis config:', choices=[('6','No Change'),
        ('0','TLV'), 
        ('1','TVL'), 
        ('2','LTV'), 
        ('3','LVT'), 
        ('4','VTL'), 
        ('5','VLT')], coerce=int, validators=[Optional()])
    bb_data_erase = BooleanField('Erase Bumpbox data:', validators=[Optional()])      # command only

class WarwickForm(Form):
    ww_enable = SelectField('Enable Warwick Wireless:', choices=[('0','No Change'),('1','true'), ('2','false')], coerce=int, validators=[Optional()])
    ww_no_reply_timeout = IntegerField('No reply timeout (1- 3600 secs):', validators=[NumberRange(min=10, max=3600), Optional()])
    ww_time_interval = IntegerField('GSM time interval (1 - 48 hrs):', validators=[NumberRange(min=1, max=48), Optional()])

class WifiForm(Form):
    wifi_filter_1 = StringField('Filter 1:', validators=[Optional(), filter_check])
    wifi_filter_2 = StringField('Filter 2:', validators=[Optional(), filter_check])
    wifi_filter_3 = StringField('Filter 3:', validators=[Optional(), filter_check])
    wifi_filter_4 = StringField('Filter 4:', validators=[Optional(), filter_check])
    wifi_shed_filter = StringField('Shed detection filter:', validators=[Optional(), filter_check])
    wifi_shed_start_time = IntegerField('On shed start time (0 - 23 hr):', validators=[NumberRange(min=0, max=23), Optional()])
    wifi_shed_end_time = IntegerField('Off shed start time (0 - 23 hr):', validators=[NumberRange(min=0, max=23), Optional()])
    wifi_shed_detection_threshold = IntegerField('Shed detection threshold (1 - 255 scans):', validators=[NumberRange(min=1, max=255), Optional()])
    wifi_scan_found_time = IntegerField('Found scan interval, secs:', validators=[Optional()])
    wifi_scan_not_found_time = IntegerField('Not found scan interval, secs:', validators=[Optional()])

class WsnForm(Form):
    wsn_data_decimation = IntegerField('Data decimation (0 - 6 mins):', validators=[NumberRange(min=0, max=6), Optional()])
    wsn_msg_conversion = SelectField('Message Conversion:', choices=[('0','No Change'),('1','true'), ('2','false')], coerce=int, validators=[Optional()])
    wsn_lora_freq = IntegerField('LORA radio frequency, Hz:', validators=[Optional()])
    wsn_lora_freq_token = SelectField('LORA radio frequency token:', choices=[('3','No Change'),
        ('0','EU Frequency'),
        ('1','USA Frequency'),
        ('1','AUS Frequency')], coerce=int, validators=[Optional()])
    wsn_list = BooleanField('List all WSNs:', validators=[Optional()])               # command only
    wsn_delete_all = BooleanField('Delete all WSNs:', validators=[Optional()])       # command only
    wsn_add_field = FieldList(StringField('ID', validators=[Optional()]), 'Add WSNs (max 48):', min_entries=1, max_entries=48)
    wsn_id_add_1 = StringField('1',validators=[Optional(), device_id_check])
    wsn_id_add_2 = StringField('2',validators=[Optional(), device_id_check])
    wsn_id_add_3 = StringField('3',validators=[Optional(), device_id_check])
    wsn_id_add_4 = StringField('4',validators=[Optional(), device_id_check])
    wsn_id_add_5 = StringField('5',validators=[Optional(), device_id_check])
    wsn_id_add_6 = StringField('6',validators=[Optional(), device_id_check])
    wsn_id_add_7 = StringField('7',validators=[Optional(), device_id_check])
    wsn_id_add_8 = StringField('8',validators=[Optional(), device_id_check])
    wsn_id_add_9 = StringField('9',validators=[Optional(), device_id_check])
    wsn_id_add_10 = StringField('10',validators=[Optional(), device_id_check])
    wsn_id_add_11 = StringField('11',validators=[Optional(), device_id_check])
    wsn_id_add_12 = StringField('12',validators=[Optional(), device_id_check])
    wsn_id_add_13 = StringField('13',validators=[Optional(), device_id_check])
    wsn_id_add_14 = StringField('14',validators=[Optional(), device_id_check])
    wsn_id_add_15 = StringField('15',validators=[Optional(), device_id_check])
    wsn_id_add_16 = StringField('16',validators=[Optional(), device_id_check])
    wsn_id_add_17 = StringField('17',validators=[Optional(), device_id_check])
    wsn_id_add_18 = StringField('18',validators=[Optional(), device_id_check])
    wsn_id_add_19 = StringField('19',validators=[Optional(), device_id_check])
    wsn_id_add_20 = StringField('20',validators=[Optional(), device_id_check])
    wsn_id_add_21 = StringField('21',validators=[Optional(), device_id_check])
    wsn_id_add_22 = StringField('22',validators=[Optional(), device_id_check])
    wsn_id_add_23 = StringField('23',validators=[Optional(), device_id_check])
    wsn_id_add_24 = StringField('24',validators=[Optional(), device_id_check])
    wsn_id_add_25 = StringField('25',validators=[Optional(), device_id_check])
    wsn_id_add_26 = StringField('26',validators=[Optional(), device_id_check])
    wsn_id_add_27 = StringField('27',validators=[Optional(), device_id_check])
    wsn_id_add_28 = StringField('28',validators=[Optional(), device_id_check])
    wsn_id_add_29 = StringField('29',validators=[Optional(), device_id_check])
    wsn_id_add_30 = StringField('30',validators=[Optional(), device_id_check])
    wsn_id_add_31 = StringField('31',validators=[Optional(), device_id_check])
    wsn_id_add_32 = StringField('32',validators=[Optional(), device_id_check])
    wsn_id_add_33 = StringField('33',validators=[Optional(), device_id_check])
    wsn_id_add_34 = StringField('34',validators=[Optional(), device_id_check])
    wsn_id_add_35 = StringField('35',validators=[Optional(), device_id_check])
    wsn_id_add_36 = StringField('36',validators=[Optional(), device_id_check])
    wsn_id_add_37 = StringField('37',validators=[Optional(), device_id_check])
    wsn_id_add_38 = StringField('38',validators=[Optional(), device_id_check])
    wsn_id_add_39 = StringField('39',validators=[Optional(), device_id_check])
    wsn_id_add_40 = StringField('40',validators=[Optional(), device_id_check])
    wsn_id_add_41 = StringField('41',validators=[Optional(), device_id_check])
    wsn_id_add_42 = StringField('42',validators=[Optional(), device_id_check])
    wsn_id_add_43 = StringField('43',validators=[Optional(), device_id_check])
    wsn_id_add_44 = StringField('44',validators=[Optional(), device_id_check])
    wsn_id_add_45 = StringField('45',validators=[Optional(), device_id_check])
    wsn_id_add_46 = StringField('46',validators=[Optional(), device_id_check])
    wsn_id_add_47 = StringField('47',validators=[Optional(), device_id_check])
    wsn_id_add_48 = StringField('48',validators=[Optional(), device_id_check])

    wsn_id_del_1 = StringField('1',validators=[Optional(), device_id_check])
    wsn_id_del_2 = StringField('2',validators=[Optional(), device_id_check])
    wsn_id_del_3 = StringField('3',validators=[Optional(), device_id_check])
    wsn_id_del_4 = StringField('4',validators=[Optional(), device_id_check])
    wsn_id_del_5 = StringField('5',validators=[Optional(), device_id_check])
    wsn_id_del_6 = StringField('6',validators=[Optional(), device_id_check])
    wsn_id_del_7 = StringField('7',validators=[Optional(), device_id_check])
    wsn_id_del_8 = StringField('8',validators=[Optional(), device_id_check])
    wsn_id_del_9 = StringField('9',validators=[Optional(), device_id_check])
    wsn_id_del_10 = StringField('10',validators=[Optional(), device_id_check])
    wsn_id_del_11 = StringField('11',validators=[Optional(), device_id_check])
    wsn_id_del_12 = StringField('12',validators=[Optional(), device_id_check])
    wsn_id_del_13 = StringField('13',validators=[Optional(), device_id_check])
    wsn_id_del_14 = StringField('14',validators=[Optional(), device_id_check])
    wsn_id_del_15 = StringField('15',validators=[Optional(), device_id_check])
    wsn_id_del_16 = StringField('16',validators=[Optional(), device_id_check])
    wsn_id_del_17 = StringField('17',validators=[Optional(), device_id_check])
    wsn_id_del_18 = StringField('18',validators=[Optional(), device_id_check])
    wsn_id_del_19 = StringField('19',validators=[Optional(), device_id_check])
    wsn_id_del_20 = StringField('20',validators=[Optional(), device_id_check])
    wsn_id_del_21 = StringField('21',validators=[Optional(), device_id_check])
    wsn_id_del_22 = StringField('22',validators=[Optional(), device_id_check])
    wsn_id_del_23 = StringField('23',validators=[Optional(), device_id_check])
    wsn_id_del_24 = StringField('24',validators=[Optional(), device_id_check])
    wsn_id_del_25 = StringField('25',validators=[Optional(), device_id_check])
    wsn_id_del_26 = StringField('26',validators=[Optional(), device_id_check])
    wsn_id_del_27 = StringField('27',validators=[Optional(), device_id_check])
    wsn_id_del_28 = StringField('28',validators=[Optional(), device_id_check])
    wsn_id_del_29 = StringField('29',validators=[Optional(), device_id_check])
    wsn_id_del_30 = StringField('30',validators=[Optional(), device_id_check])
    wsn_id_del_31 = StringField('31',validators=[Optional(), device_id_check])
    wsn_id_del_32 = StringField('32',validators=[Optional(), device_id_check])
    wsn_id_del_33 = StringField('33',validators=[Optional(), device_id_check])
    wsn_id_del_34 = StringField('34',validators=[Optional(), device_id_check])
    wsn_id_del_35 = StringField('35',validators=[Optional(), device_id_check])
    wsn_id_del_36 = StringField('36',validators=[Optional(), device_id_check])
    wsn_id_del_37 = StringField('37',validators=[Optional(), device_id_check])
    wsn_id_del_38 = StringField('38',validators=[Optional(), device_id_check])
    wsn_id_del_39 = StringField('39',validators=[Optional(), device_id_check])
    wsn_id_del_40 = StringField('40',validators=[Optional(), device_id_check])
    wsn_id_del_41 = StringField('41',validators=[Optional(), device_id_check])
    wsn_id_del_42 = StringField('42',validators=[Optional(), device_id_check])
    wsn_id_del_43 = StringField('43',validators=[Optional(), device_id_check])
    wsn_id_del_44 = StringField('44',validators=[Optional(), device_id_check])
    wsn_id_del_45 = StringField('45',validators=[Optional(), device_id_check])
    wsn_id_del_46 = StringField('46',validators=[Optional(), device_id_check])
    wsn_id_del_47 = StringField('47',validators=[Optional(), device_id_check])
    wsn_id_del_48 = StringField('48',validators=[Optional(), device_id_check])

class ConfigForm(FlaskForm):

    dc_id = StringField('DC ID:', validators=[Required(), device_id_check])

    system_fields = FormField(SystemForm)
    comms_fields = FormField(CommsForm)
    bumpbox_fields = FormField(BumpboxForm)
    warwick_fields = FormField(WarwickForm)
    wifi_fields = FormField(WifiForm)
    wsn_fields = FormField(WsnForm)

    submit = SubmitField('Save Config')

