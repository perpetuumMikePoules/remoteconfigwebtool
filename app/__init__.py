# a sub-directory that includes a __init__.py file is considered a package, and can be imported.
# When a package is imported, the __init__.py executes and defines what sysmbols the package exposes to the outside world

from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap



# create instance of flask
rct = Flask(__name__)

# CSS framework
bootstrap = Bootstrap(rct)

# read flask config and apply
rct.config.from_object(Config)

# create database instance
db = SQLAlchemy(rct)
# create database migration instance
migrate = Migrate(rct, db)

# get development build state
DEV_BUILD = Config.DEV_BUILD

# ALWAYS at bottom, to prevent circular imports
# import modules:
#   routes - defines URL's view functions
#   models - defines structure of database
from app import routes, models
