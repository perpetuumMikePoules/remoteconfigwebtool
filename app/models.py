from datetime import datetime
from app import db


class DcInfo(db.Model):
    # db fields are defined as class variable instances of db.Column
    id = db.Column(db.Integer, primary_key=True)
    dc_id_num = db.Column(db.String(19), index=True, unique=True)
    firmware_ver = db.Column(db.String(8), index=True, unique=True)
    last_update_time = db.Column(db.DateTime, default=datetime.utcnow)
    # one to one relationship
    config = db.relationship('DcConfig', uselist=False, backref='dc_info')

    # tells python how to print objects of this class - useful for debugging
    def __repr__(self):
        return '<DC Id: {}>'.format(self.dc_id_num)

class DcConfig(db.Model):
    # db fields are defined as class variable instances of db.Column

    id = db.Column(db.Integer, primary_key=True)
    # system fields
    sd_card = db.Column(db.Boolean, unique=True)                        # True/False
    log_event_enable = db.Column(db.Integer, unique=True)               # 0x0000 = Normal, 0x0001 = WSN sniffing, 0x0002 = GSM operator sniffing
    remote_restart = db.Column(db.Boolean, unique=True)                 # command only
    stored_data_erase = db.Column(db.Boolean, unique=True)              # command only
    # comms fields
    azure_setting = db.Column(db.Integer, unique=True)                  # 0 = perpetuum, 1 = special
    access_point = db.Column(db.String(100), unique=True)               # name[40], address[30], username[15], password[15]
    bb_blocks_per_chunk = db.Column(db.Integer, unique=True)            # 1 - 5
    single_endpoint = db.Column(db.Boolean, unique=True)                # True/False
    data_upload_window = db.Column(db.Integer, unique=True)             # 5 - 55
    modem_highspeed = db.Column(db.Boolean, unique=True)                # True/False
    # bumpbox
    bb_gps_enable  = db.Column(db.Boolean, unique=True)                 # True/False
    bb_data_enable  = db.Column(db.Boolean, unique=True)                # True/False
    bb_data_type = db.Column(db.Integer, unique=True)                   # 2 - 14 (not 3)
    bb_decimation = db.Column(db.Integer, unique=True)                  # 1 (off) - 255
    bb_accel_axis_config = db.Column(db.Integer, unique=True)           # 0 - 5
    bb_data_erase = db.Column(db.Boolean, unique=True)                  # command only
    # warwick wireless
    ww_enable  = db.Column(db.Boolean, unique=True)                     # True/False
    ww_no_reply_timeout = db.Column(db.Integer, unique=True)            # 10 - 3600
    ww_time_interval = db.Column(db.Integer, unique=True)               # 1 - 48
    # wifi
    wifi_filter_1 = db.Column(db.String(17), unique=True)               # octets format - nn:nn:nn:nn:nn:nn
    wifi_filter_2 = db.Column(db.String(17), unique=True)               # octets format - nn:nn:nn:nn:nn:nn
    wifi_filter_3 = db.Column(db.String(17), unique=True)               # octets format - nn:nn:nn:nn:nn:nn
    wifi_filter_4 = db.Column(db.String(17), unique=True)               # octets format - nn:nn:nn:nn:nn:nn
    wifi_shed_filter = db.Column(db.String(17), unique=True)            # octets format - nn:nn:nn:nn:nn:nn
    wifi_shed_start_time = db.Column(db.Integer, unique=True)           # 0 - 23
    wifi_shed_end_time = db.Column(db.Integer, unique=True)             # 0 - 23
    wifi_shed_detection_threshold = db.Column(db.Integer, unique=True)  # 1 - 255
    wifi_scan_found_time = db.Column(db.Integer, unique=True)           # secs
    wifi_scan_not_found_time = db.Column(db.Integer, unique=True)       # secs
    # wsn
    wsn_add_ids = db.Column(db.String(960), unique=True)                # 48 x "nnnn-nnnn-nnnn-nnnn;"
    wsn_delete_ids = db.Column(db.String(960), unique=True)             # 48 x "nnnn-nnnn-nnnn-nnnn;"
    wsn_delete_all = db.Column(db.Boolean, unique=True)                 # command only
    wsn_list = db.Column(db.Boolean, unique=True)                       # command only
    wsn_data_decimation = db.Column(db.Integer, unique=True)            # 0 - 6
    wsn_msg_conversion = db.Column(db.Boolean, unique=True)             # True/False
    # lora frequency
    wsn_lora_freq = db.Column(db.Integer, unique=True)                  # in MHz
    wsn_lora_freq_token = db.Column(db.Integer, unique=True)            # 0 - 2

    # Foreign key - link back to DC Info
    dc_id = db.Column(db.Integer, db.ForeignKey('dc_info.id'))

    # tells python how to print objects of this class - useful for debugging
    def __repr__(self):
        return '<SD Card enable {}>'.format(self.sd_card)

