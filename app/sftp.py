import os
from config import Config
import pysftp

class ConfigSftp():

    # select server address - use local host if Dev Build    
    if Config.DEV_BUILD:
        __cfg_host = "192.168.56.1"
        __cfg_username = "tester"
        __cfg_password = "password"
        __cfg_directory = "/var/www/html"
    else:
        __cfg_host = "10.0.1.210"
        __cfg_username = "softdev"
        __cfg_password = "Hythe00)"
        __cfg_directory = "/var/www/html"
    __cfg_port = 22

    def upload_config_files(self, local_file_path: str, local_file_1: str, local_file_2: str) -> None:

        # turns off host key - only use in development as dangerous
        if Config.DEV_BUILD:
            cnopts = pysftp.CnOpts()
            cnopts.hostkeys = None

        # make connection do DC config server - port not re quired as default is 22
        with pysftp.Connection(host=self.__cfg_host, username=self.__cfg_username, password=self.__cfg_password, cnopts=cnopts) as sftp:
            print("Connection Established")

            # build remote directory paths
            __remote_file_1_path: str = os.path.join(self.__cfg_directory, local_file_1)
            __remote_file_2_path: str = os.path.join(self.__cfg_directory, local_file_2)
            # copy .txt & .cfg files to server
            sftp.put(os.path.join(local_file_path, local_file_1), __remote_file_1_path)
            sftp.put(os.path.join(local_file_path, local_file_2), __remote_file_2_path)
