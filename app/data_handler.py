import os
import time
import base64
from app import rct
from app.models import DcConfig
from config import Config
from app.sftp import ConfigSftp

from typing import List
from app.forms import ConfigForm

class DataHandler():

    def save_changes(self, __text_list: List[str], dc_id: str) -> None:
        # create dir in known location if doesn't already exist
        __save_dir: str = os.path.join(rct.instance_path, 'savedConfigs')
        os.makedirs(__save_dir, exist_ok=True)

        # save .txt file to local folder - file format = dcid + _ + save time + .txt
        __txt_filename: str = dc_id + '_{}'.format(int(time.time())) + '.' + 'txt'
        __txt_file_path: str = os.path.join(__save_dir, __txt_filename)
        with open(__txt_file_path, "w") as fo:
            fo.writelines("%s" % text for text in __text_list)

        # convert .txt file to base 64 .cfg file and save in local folder
        __list_str: List[str] = ''.join([str(elm) for elm in __text_list])
        __b64_str: str = base64.b64encode(__list_str.encode('ascii'))
        __b64_filename: str = dc_id + '.' +'cfg' 
        __b64_file_path: str = os.path.join(__save_dir, __b64_filename) 
        with open(__b64_file_path, "wb") as fo:
            fo.write(__b64_str)

        # copy saved config files to Config Server
        config_sftp: ConfigSftp = ConfigSftp()
        config_sftp.upload_config_files(__save_dir, __txt_filename, __b64_filename)

    def process_add_wsn(self, wsn_id: str, text_list: List[str]) -> None:
        if wsn_id != '':
            text_list.append('WSNADD:{}\n'.format(wsn_id.upper()))

    def process_del_wsn(self, wsn_id: str, text_list: List[str]) -> None:
        if wsn_id != '':
            text_list.append('WSNDEL:{}\n'.format(wsn_id.upper()))
    
    def handle_config_form(self, form: ConfigForm) -> None:
        __text_list: List[str] = []

        # opening element
        __text_list.append('SYSGMT:{}\n'.format(int(time.time())))

        # system fields
        if form.system_fields.sd_card.data != 0:
            __text_list.append('SDCARD:{}\n'.format('true' if form.system_fields.sd_card.data == 1 else 'false'))
        
        if form.system_fields.log_event_enable.data != 3:
            __text_list.append('LOGENA:{}\n'.format(form.system_fields.log_event_enable.data))

        if form.system_fields.remote_restart.data == True:
            __text_list.append('SYSRST\n')

        if form.system_fields.stored_data_erase.data == True:
            __text_list.append('SYSERS\n')

        # comms fields
        if form.comms_fields.azure_setting.data != 3:
            __text_list.append('CLOUDC:{}\n'.format(form.comms_fields.azure_setting.data))

        if form.comms_fields.access_point.data != '':
            __text_list.append('APNOR:{}\n'.format(form.comms_fields.access_point.data))

        if form.comms_fields.single_endpoint.data != 0:
            __text_list.append('SGLEND:{}\n'.format('true' if form.comms_fields.single_endpoint.data == 1 else 'false'))
        
        if form.comms_fields.data_upload_window.data != None:
            __text_list.append('GSMCCW:{}\n'.format(form.comms_fields.data_upload_window.data))

        if form.comms_fields.modem_highspeed.data != 0:
            __text_list.append('RATHSE:{}\n'.format('true' if form.comms_fields.single_endpoint.data == 1 else 'false'))

        if form.comms_fields.bb_blocks_per_chunk.data != None:
            __text_list.append('BLKNUM:{}\n'.format(form.comms_fields.bb_blocks_per_chunk.data))

        # bumpbox
        if form.bumpbox_fields.bb_gps_enable.data != 0:
            __text_list.append('BMPGPS:{}\n'.format('true' if form.bumpbox_fields.bb_gps_enable.data == 1 else 'false'))

        if form.bumpbox_fields.bb_data_enable.data != 0:
            __text_list.append('BMPDAT:{}\n'.format('true' if form.bumpbox_fields.bb_data_enable.data == 1 else 'false'))

        if form.bumpbox_fields.bb_data_type.data != 0:
            __text_list.append('BMPDAT:{}\n'.format(form.bumpbox_fields.bb_data_type.data))

        if form.bumpbox_fields.bb_decimation.data != None:
            __text_list.append('BMPDCP:{}\n'.format(form.bumpbox_fields.bb_decimation.data))

        if form.bumpbox_fields.bb_accel_axis_config.data != 6:
            __text_list.append('BMPDAT:{}\n'.format(form.bumpbox_fields.bb_accel_axis_config.data))

        if form.bumpbox_fields.bb_data_erase.data == True:
            __text_list.append('BMPERS\n')

        # warwick radio
        if form.warwick_fields.ww_enable.data != 0:
            __text_list.append('WR_ENA:{}\n'.format('true' if form.warwick_fields.ww_enable.data == 1 else 'false'))

        if form.warwick_fields.ww_no_reply_timeout.data != None:
            __text_list.append('WR_TNR:{}\n'.format(form.warwick_fields.ww_no_reply_timeout.data))

        if form.warwick_fields.ww_time_interval.data != None:
            __text_list.append('WR_TIN:{}\n'.format(form.warwick_fields.ww_time_interval.data))

        # WiFi
        if form.wifi_fields.wifi_filter_1.data != '':
            __text_list.append('WIFIFILTER1:{}\n'.format(form.wifi_fields.wifi_filter_1.data))

        if form.wifi_fields.wifi_filter_2.data != '':
            __text_list.append('WIFIFILTER2:{}\n'.format(form.wifi_fields.wifi_filter_2.data))

        if form.wifi_fields.wifi_filter_3.data != '':
            __text_list.append('WIFIFILTER3:{}\n'.format(form.wifi_fields.wifi_filter_3.data))

        if form.wifi_fields.wifi_filter_4.data != '':
            __text_list.append('WIFIFILTER4:{}\n'.format(form.wifi_fields.wifi_filter_4.data))

        if form.wifi_fields.wifi_shed_filter.data != '':
            __text_list.append('WIFIFILTERSHED:{}\n'.format(form.wifi_fields.wifi_shed_filter.data))

        if form.wifi_fields.wifi_shed_start_time.data != None:
            __text_list.append('WIFISHEDSTART:{}\n'.format(form.wifi_fields.wifi_shed_start_time.data))

        if form.wifi_fields.wifi_shed_end_time.data != None:
            __text_list.append('WIFISHEDEND:{}\n'.format(form.wifi_fields.wifi_shed_end_time.data))

        if form.wifi_fields.wifi_shed_detection_threshold.data != None:
            __text_list.append('WIFISHEDTHR:{}\n'.format(form.wifi_fields.wifi_shed_detection_threshold.data))

        if form.wifi_fields.wifi_scan_found_time.data != None:
            __text_list.append('WIFISCANFOUND:{}\n'.format(form.wifi_fields.wifi_scan_found_time.data))

        if form.wifi_fields.wifi_scan_not_found_time.data != None:
            __text_list.append('WIFISCANNOTFOUND:{}\n'.format(form.wifi_fields.wifi_scan_not_found_time.data))

        # WSN 
        if form.wsn_fields.wsn_data_decimation.data != None:
            __text_list.append('DATDEC:{}\n'.format(form.wsn_fields.wsn_data_decimation.data))

        if form.wsn_fields.wsn_msg_conversion.data != 0:
            __text_list.append('MSGCNG:{}\n'.format('true' if form.wsn_fields.wsn_msg_conversion.data == 1 else 'false'))

        if form.wsn_fields.wsn_lora_freq.data != None:
            __text_list.append('LORAFQ:{}\n'.format(form.wsn_fields.wsn_lora_freq.data))

        if form.wsn_fields.wsn_lora_freq_token.data != 3:
            __text_list.append('LORAFT:{}\n'.format(form.wsn_fields.wsn_lora_freq_token.data))

        if form.wsn_fields.wsn_list.data == True:
            __text_list.append('WSNLST\n')

        if form.wsn_fields.wsn_delete_all.data == True:
            __text_list.append('WSNDELALL\n')

        self.process_add_wsn(form.wsn_fields.wsn_id_add_1.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_2.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_3.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_4.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_5.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_6.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_7.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_8.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_9.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_10.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_11.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_12.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_13.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_14.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_15.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_16.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_17.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_18.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_19.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_20.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_21.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_22.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_23.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_24.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_25.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_26.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_27.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_28.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_29.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_30.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_31.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_32.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_33.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_34.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_35.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_36.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_37.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_38.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_39.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_40.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_41.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_42.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_43.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_44.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_45.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_46.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_47.data, __text_list)
        self.process_add_wsn(form.wsn_fields.wsn_id_add_48.data, __text_list)

        self.process_del_wsn(form.wsn_fields.wsn_id_del_1.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_2.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_3.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_4.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_5.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_6.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_7.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_8.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_9.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_10.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_21.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_22.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_23.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_24.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_25.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_26.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_27.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_28.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_29.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_31.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_32.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_33.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_34.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_35.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_36.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_37.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_38.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_39.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_41.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_42.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_43.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_44.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_45.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_46.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_47.data, __text_list)
        self.process_del_wsn(form.wsn_fields.wsn_id_del_48.data, __text_list)

        # closing element
        __text_list.append('SYSEND\n')

        # save changes to file - DC ID.txt
        self.save_changes(__text_list, form.dc_id.data.upper())    

        # debug outputs changes to terminal
        if Config.DEV_BUILD:
            print(len(__text_list))
            for text_str in __text_list:
                print(text_str)


