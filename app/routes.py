from flask import render_template, flash, redirect, url_for, request
from app import rct
from app.forms import ConfigForm
from app.models import DcInfo, DcConfig, DcInfo
from app.data_handler import DataHandler

# map rct's '/' & '/index' URLs to view function 'index()'
@rct.route('/')
@rct.route('/index')
def index():
    return render_template('index.html',title='Rmote Config Tool')


# GET requests - return info to web browser (client), 
# POST requests - browser submits form data to server
@rct.route('/dc_config', methods=['GET','POST'])
def config():
    # instatiate ConfigForm object
    form = ConfigForm()
    # validate_on_submit returns False for GET & True for POST if all validators OK
    if request.method == 'POST':
        if form.validate_on_submit():
            # handle POST request
            dh = DataHandler()
            dh.handle_config_form(form)

            return redirect(url_for('index'))
        else:
            return render_template('dc_config.html', title='DC Configuration', form=form)

    elif request.method == 'GET':
        # handle GET request
        return render_template('dc_config.html', title='DC Configuration', form=form)

